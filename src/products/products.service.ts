/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { NotFoundError } from 'rxjs';

let products: Product[] = [
  { id: 1, name: 'Product1', price: 200 },
  { id: 2, name: 'Product2', price: 300 },
];
let lastProductId = 3;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct: Product = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'Product1', price: 200 },
      { id: 2, name: 'Product2', price: 300 },
    ];
    lastProductId = 3;
    return 'RESET';
  }
}
